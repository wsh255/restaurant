<?php


namespace app\index\controller;


use think\Template;

class Index
{
    private function fetch($path)
    {
        $config = [
            'view_path' => app_path() . 'views/',
            'cache_path' => './runtime/',
            'view_suffix' => 'html',
        ];
        $template = new Template($config);
        return $template->fetch($path);
    }

    public function index()
    {
        return $this->fetch('index:index');
    }

    public function lists()
    {
        return $this->fetch('index:lists');
    }

    public function lists_join()
    {
        return $this->fetch('index:lists_join');
    }

    public function lists_filter()
    {
        return $this->fetch('index:lists_filter');
    }

    public function lists_page()
    {
        return $this->fetch('index:lists_page');
    }

    public function order()
    {
        return $this->fetch('index:lists_order');
    }

    public function hide_filed()
    {
        return $this->fetch('index:list_hide_field');
    }


    public function lists_action()
    {
        return $this->fetch('index:lists_action');
    }

    public function fields()
    {
        return $this->fetch('index:fields');
    }

    public function soft_delete()
    {
        return $this->fetch('index:soft_delete');
    }

    public function custom_list_template()
    {
        return $this->fetch('index:custom_list_template');
    }

    public function add_resources()
    {
        return $this->fetch('index:add_resources');
    }

    public function list_field()
    {
        return $this->fetch('index:list_field');
    }

    public function custom_field()
    {
        return $this->fetch('index:custom_field');
    }

    public function form_add_field()
    {
        return $this->fetch('index:form_add_field');
    }

    public function form_add_action()
    {
        return $this->fetch('index:form_add_action');
    }

    public function form_custom_template()
    {
        return $this->fetch('index:form_custom_template');
    }

    public function btn()
    {
        return $this->fetch('index:btn');
    }

    public function template_extend()
    {
        return $this->fetch('index:template_extend');
    }

    public function show()
    {
        return $this->fetch('index:show');
    }


    public function show_action()
    {
        return $this->fetch('index:show_action');
    }

    public function show_join()
    {
        return $this->fetch('index:show_join');
    }

    public function custom_page()
    {
        return $this->fetch('index:custom_page');
    }

    public function rewrite_method()
    {
        return $this->fetch('index:rewrite_method');
    }

    public function breadcrumb()
    {
        return $this->fetch('index:breadcrumb');
    }

    public function nav()
    {
        return $this->fetch('index:nav');
    }

    public function form_field()
    {
        return $this->fetch('index:form_field');
    }

    public function qa()
    {
        return $this->fetch('index:qa');
    }

    public function actions()
    {
        return $this->fetch('index:actions');
    }

    public function verify()
    {
        return $this->fetch('index:verify');
    }

    public function list_edit()
    {
        return $this->fetch('index:lists_edit');
    }

    public function assign()
    {
        return $this->fetch('index:assign');
    }

    public function login()
    {
        return $this->fetch('index:login');
    }


    public function intro()
    {
        return $this->fetch('index:intro');
    }

    public function list_query()
    {
        return $this->fetch('index:list_query');
    }

    public function role()
    {
        return $this->fetch('index:role');
    }

}
