<?php

namespace app\admin\controller;


use easyadmin\app\columns\form\FormAutocomplete;
use easyadmin\app\columns\form\FormCurrency;
use easyadmin\app\columns\form\FormEditor;
use easyadmin\app\columns\form\FormNumber;
use easyadmin\app\columns\form\FormSelect;
use easyadmin\app\columns\form\FormText;
use easyadmin\app\columns\form\FormUpload;
use easyadmin\app\columns\lists\ListImage;
use easyadmin\app\columns\lists\ListSwitch;
use easyadmin\app\columns\lists\ListText;
use easyadmin\app\libs\ListField;
use easyadmin\app\libs\Page;
use easyadmin\app\libs\PageForm;
use think\db\Query;
use think\facade\Db;

class Food extends Admin
{


    protected string $softDeleteField = 'is_del';
    protected string $pageName = '菜品列表';


    protected function configListJoin(Page $page, Query $query, string $alias)
    {
        $query->join('store s', "s.id={$alias}.store_id", 'left');
        $query->join('tag t', "t.id={$alias}.tag_id", 'left');
        $query->join('category c', "c.id={$alias}.category_id", 'left');
    }

    protected function configListField(ListField $list)
    {
        $list
            ->addField('id', 'ID', ListText::class)
            ->addField('s.name', '商户', ListText::class, ['default' => '-'])
            ->addField('c.name', '分类', ListText::class, ['default' => '-'])
            ->addField('t.name', '标签', ListText::class, ['default' => '-'])
            ->addField('img', '图片', ListImage::class)
            ->addField('name', '名称', ListText::class)
            ->addField('intro', '一句话描述', ListText::class)
            ->addField('price', '价格', ListText::class)
            ->addField('discount_price', '折扣价格', ListText::class, [
                'template' => 'food:discount_price'
            ])
            ->addField('discount_limit', '折扣限购', ListText::class)
            ->addField('sort', '排序', ListText::class,[
                'edit'=>true,
                'attr'=>'style=width:60px', //设置输入框宽度
                // 下列选项 edit=true 有效
                'type'=>'number',   // 设置输入框类型, 非必须, 默认 text
                'url'=>'enable' , // 设置请求URL ,  非必须, 默认 enable  (所有继承 Admin 类的控制器都有这个方法)
                'params'=>[] ,    // 设置请求的参数, 非必须, 默认 空数组
            ])
            ->addField('is_enable', '是否上线', ListSwitch::class);
    }


    protected function configFormField(PageForm $page)
    {
        $page->setTemplate('food:form');

        //查询用户的额口味
        $this->getFlavor();

        $page
            ->addField('store_id', '商户', FormAutocomplete::class, [
                'table' => 'store',
                'pk' => 'id',
                'property' => 'name',
                'required' => true,
            ])
            ->addField('category_id', '分类', FormSelect::class, [
                'table' => 'category',
                'pk' => 'id',
                'property' => 'name',
            ])
            ->addField('tag_id', '标签', FormSelect::class, [
                'table' => 'tag',
                'pk' => 'id',
                'property' => 'name',
            ])
            ->addField('img', '图片', FormUpload::class, [
                'required' => true,
            ])
            ->addField('name', '名称', FormText::class, ['required' => true,])
            ->addField('intro', '一句话描述', FormText::class, [])
            ->addField('price', '价格', FormCurrency::class, [])
            ->addField('discount_price', '折扣价格', FormCurrency::class, [])
            ->addField('discount_limit', '折扣限购数量', FormNumber::class, [])
            ->addField('sort', '排序', FormCurrency::class, [])
            ->addField('desc', '详细描述', FormEditor::class, []);
    }

    protected function insertAfter($data): array
    {
        $this->saveFlavor($data['id']);
        return $data;
    }

    protected function updateAfter($data): array
    {
        $this->saveFlavor($data['id']);
        return $data;
    }

    /**
     * 写入菜品口味
     * @param $foodId
     * @throws \think\db\exception\DbException
     */
    protected function saveFlavor($foodId)
    {
        $flavorIds = request()->post('food_flavor');
        if (empty($flavorIds)) return;

        Db::name('food_flavor')->where('food_id', '=', $foodId)->delete();

        $data = [];
        foreach ($flavorIds as $flavorId) {
            array_push($data, [
                'food_id' => $foodId,
                'flavor_id' => $flavorId,
            ]);
        }

        Db::name('food_flavor')->insertAll($data);
    }


    protected function getFlavor()
    {

        //口味查询好,赋值到列表
        $this->assign('flavor', Db::name('flavor')->select());

        //查询用户已经选择好的口味
        $food_id = request()->get('id');
        if ($food_id) {
            $selectFlavor = Db::name('food_flavor')->where('food_id', '=', $food_id)->select()->toArray();
            if ($selectFlavor) {
                $selectFlavor = array_column($selectFlavor, 'flavor_id');
                $this->assign('selectFlavor', $selectFlavor);
            }
        }

    }


}
