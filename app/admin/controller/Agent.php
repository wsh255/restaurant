<?php

namespace app\admin\controller;


use easyadmin\app\columns\form\FormDateTime;
use easyadmin\app\columns\form\FormEditor;
use easyadmin\app\columns\form\FormSelect;
use easyadmin\app\columns\form\FormText;
use easyadmin\app\columns\form\FormTextarea;
use easyadmin\app\columns\form\FormUpload;
use easyadmin\app\columns\lists\ListDateTime;
use easyadmin\app\columns\lists\ListImage;
use easyadmin\app\columns\lists\ListSelect;
use easyadmin\app\columns\lists\ListText;
use easyadmin\app\libs\Btn;
use easyadmin\app\libs\ListField;
use easyadmin\app\libs\ListFilter;
use easyadmin\app\libs\PageForm;
use easyadmin\app\libs\PageList;
use easyadmin\app\libs\Pagination;
use easyadmin\app\libs\Verify;

class Agent extends Admin
{

    protected string $softDeleteField = 'is_del';
    protected string $pageName = '代理商';

    protected $levels = [
        ['key' => '1', 'text' => '普通代理'],
        ['key' => '2', 'text' => '铜牌代理'],
        ['key' => '3', 'text' => '银牌代理'],
        ['key' => '4', 'text' => '金牌代理'],
    ];


    protected function configList(PageList $page)
    {
        $page
            ->addAction('商户列表', 'admin/store/lists', [
                'icon' => 'layui-icon layui-icon-release',
                'class' => ['layui-btn-primary', 'layui-btn-xs'],
                'format'=>function($row){
                    return url('admin/store/lists',['agent_id'=>$row['id']]);
                }
            ])
            ->addAction('编辑', 'edit', [
                'icon' => 'layui-icon layui-icon-edit',
                'class' => ['layui-btn-primary', 'layui-btn-xs']
            ])
            ->addAction('删除', 'delete', [
                'icon' => 'layui-icon layui-icon-delete',
                'class' => ['layui-btn-danger', 'layui-btn-xs'],
                'confirm' => '确定要删除数据吗?',
            ]);


        $addBtn = new Btn();
        $addBtn->setLabel('添加');
        $addBtn->setUrl('add');
        $addBtn->setIcon('layui-icon layui-icon-add-1');
        $page->setActionAdd($addBtn);

        //设置查询排序
        $page->setOrderBy(['id' => 'desc']);

    }

    protected function configListPagination(Pagination $page)
    {
        $page->setPageSize(4);
    }

    protected function configListField(ListField $list)
    {
        $list
            ->addField('id', 'ID', ListText::class)
            ->addField('intro_cover', '封面', ListImage::class)
            ->addField('name', '名称', ListText::class)
            ->addField('store_num', '店铺数量', ListText::class)
            ->addField('level', '代理等级', ListSelect::class, [
                'options' => $this->levels
            ])
            ->addField('contact_name', '联系人', ListText::class)
            ->addField('contact_mobile', '联系电话', ListText::class)
            ->addField('create_time', '入驻时间', ListDateTime::class)
            ->addField('expired_time', '过期时间', ListDateTime::class);
    }


    protected function configFormField(PageForm $page)
    {
        $page
            ->addField('name', '名称', FormText::class, [
                'required' => true,
                'verify' => (new Verify())
                    ->addRule('chinese', '名称请输入中文')
                    ->addRule('maxlength', '名称不能超过5个字符', 5)
                    ->addRule('minlength', '名称不能少于过2个字符', 2)
            ])
            ->addField('intro_cover', '封面', FormUpload::class)
            ->addField('level', '代理等级', FormSelect::class, [
                'options' => $this->levels
            ])
            ->addField('contact_name', '联系人', FormText::class, [
                'verify' => (new Verify())
                    ->addRule('maxlength', '联系人不能超过8个字符', 8)
                    ->addRule('minlength', '联系人不能少于过2个字符', 2)
            ])
            ->addField('contact_mobile', '联系电话', FormText::class, [
                'verify' => (new Verify())->addRule('mobile', '请输入正确的联系电话')
            ])
            ->addField('contact_qq_wx', 'QQ 微信', FormText::class, [
                'required' => true,
                // 自定义函数验证
                'verify' => function ($val) {
                    if ($val != '583161908') {
                        return 'QQ 群是 583161908, 请输入: 583161908';
                    }
                    return true;
                }
            ])
            ->addField('contact_addr', '联系地址', FormText::class, [
                'required' => true,
                'verify' => (new Verify())->addRule('reg', '请输入 重庆', '/^重庆$/')
            ])
            ->addField('expired_time', '过期时间', FormDateTime::class, [
                'in_format' => 'strtotime'
            ])
            ->addField('intro_title', '简介', FormTextarea::class)
            ->addField('intro_desc', '详细介绍', FormEditor::class);
    }

    public function configListFilter(ListFilter $filter)
    {
        $filter
            ->addFilter('name','代理商名称',FormText::class)
            ->addFilter('level', '代理等级', FormSelect::class, [
                'options' => $this->levels
            ])
            ->addFilter('contact_name','联系人',FormText::class);

    }


}
