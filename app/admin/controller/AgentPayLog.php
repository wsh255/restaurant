<?php

namespace app\admin\controller;


use easyadmin\app\columns\form\FormCurrency;
use easyadmin\app\columns\form\FormSelect;
use easyadmin\app\columns\form\FormText;
use easyadmin\app\columns\form\FormTextarea;
use easyadmin\app\columns\lists\ListDateTime;
use easyadmin\app\columns\lists\ListSelect;
use easyadmin\app\columns\lists\ListText;
use easyadmin\app\libs\Breadcrumb;
use easyadmin\app\libs\Btn;
use easyadmin\app\libs\ListField;
use easyadmin\app\libs\Page;
use easyadmin\app\libs\PageForm;
use easyadmin\app\libs\PageList;
use think\db\Query;

class AgentPayLog extends Admin
{
    protected array $disabledAction = ['delete'];

    protected string $pageName = '充值记录';

    protected $types = [
        ['key' => '1', 'text' => '支付宝'],
        ['key' => '2', 'text' => '微信'],
        ['key' => '3', 'text' => '转账'],
        ['key' => '4', 'text' => '线下付款'],
    ];
    protected $status = [
        ['key' => '0', 'text' => '待审核', 'color' => '#d2d2d2'],
        ['key' => '1', 'text' => '通过', 'color' => '#5FB878'],
        ['key' => '2', 'text' => '失败', 'color' => '#FF5722'],
    ];


    protected function configList(PageList $page)
    {
        $page
            ->addAction('编辑', 'edit', [
                'icon' => 'layui-icon layui-icon-edit',
                'class' => ['layui-btn-primary', 'layui-btn-xs']
            ]);

        $addBtn = new Btn();
        $addBtn->setLabel('添加');
        $addBtn->setUrl('add');
        $addBtn->setIcon('layui-icon layui-icon-add-1');
        $page->setActionAdd($addBtn);
    }

    protected function configListJoin(Page $page, Query $query, string $alias)
    {
        $query->join('agent a', "a.id={$alias}.agent_id");
    }


    protected function configListField(ListField $list)
    {
        $list
            ->addField('id', 'ID', ListText::class)
            ->addField('a.name', '代理商', ListText::class, ['default' => '-'])
            ->addField('money', '充值金额', ListText::class)
            ->addField('account', '支付账户', ListText::class)
            ->addField('account_name', '账户名称', ListText::class)
            ->addField('type', '充值方式', ListSelect::class, [
                'options' => $this->types
            ])
            ->addField('mark', '充值说明', ListText::class)
            ->addField('create_time', '充值时间', ListDateTime::class)
            ->addField('examine_status', '审核状态', ListSelect::class, [
                'options' => $this->status
            ])
            ->addField('examine_time', '审核时间', ListDateTime::class, [
                'default' => '-'
            ])
            ->addField('examine_mark', '审核说明', ListText::class);


    }

    protected function configListWhere(Page $page, Query $query, $alias)
    {
        $agentId = request()->get('id');
        if ($agentId) {
            $query->where("{$alias}.agent_id", '=', $agentId);

            //面包屑增加
            $breadcrumb = Breadcrumb::getInstance();
            $breadcrumb->add('代理商', request()->server('HTTP_REFERER'), '', 1);

        }
    }


    protected function configFormField(PageForm $page)
    {

        if ($this->getFormType() == 'edit') {
            $page
                ->addField('examine_status', '审核状态', FormSelect::class, [
                    'options' => $this->status
                ])
                ->addField('examine_mark', '审核说明', FormTextarea::class);
        } else {
            $page
                ->addField('agent_id', '代理商', FormSelect::class, [
                    'table' => 'agent',
                    'pk' => 'id',
                    'property' => 'name'
                ])
                ->addField('money', '充值金额', FormCurrency::class)
                ->addField('account', '支付账户', FormText::class)
                ->addField('account_name', '账户名称', FormText::class)
                ->addField('type', '充值方式', FormSelect::class, [
                    'options' => $this->types
                ])
                ->addField('flowing_water', '流水号', FormText::class)
                ->addField('mark', '充值说明', FormText::class);
        }


    }

    protected function updateBefore($data): array
    {
        $data = parent::updateBefore($data);
        $data['examine_time'] = time();
        return $data;
    }

    protected function insertBefore($data): array
    {
        $data = parent::updateBefore($data);
        $data['examine_time'] = 0;
        $data['examine_mark'] = '';
        return $data;
    }


}
