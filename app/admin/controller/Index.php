<?php

namespace app\admin\controller;


use think\db\exception\PDOException;
use think\facade\Db;

class Index extends Admin
{
    public function index()
    {
        $this->assign('name',request()->get('name','ZhangSan'));
        return $this->fetch('index:index',[
            'time'=>date('Y-m-d H:i:s')
        ]);
    }


    public function recovery_del()
    {
        $tables = Db::query("show tables");
        $tables = array_column($tables,'Tables_in_restaurant');

        foreach ($tables as $table){
            try{
                Db::query("update {$table} set is_del=0");
            }catch (PDOException $exception){

            }

        }


        return $this->fetch('index:recovery_del');
    }


}
