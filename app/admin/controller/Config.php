<?php

namespace app\admin\controller;


use app\model\Config as ConfigModel;
use easyadmin\app\columns\form\FormSelect;
use easyadmin\app\columns\form\FormText;
use easyadmin\app\columns\lists\ListDateTime;
use easyadmin\app\columns\lists\ListSwitch;
use easyadmin\app\columns\lists\ListText;
use easyadmin\app\libs\Btn;
use easyadmin\app\libs\ListField;
use easyadmin\app\libs\Page;
use easyadmin\app\libs\PageForm;
use easyadmin\app\libs\PageList;
use Exception as ExceptionAlias;
use think\db\Query;
use think\facade\Db;
use think\response\Json as JsonAlias;

class Config extends Admin
{

    protected string $softDeleteField = 'is_del';
    protected string $pageName = '系统配置';

    protected function configList(PageList $page)
    {
        $page
            ->addAction('编辑', 'edit', [
                'icon' => 'layui-icon layui-icon-edit',
                'class' => ['layui-btn-primary', 'layui-btn-xs']
            ])
            ->addAction('删除', 'delete', [
                'icon' => 'layui-icon layui-icon-delete',
                'class' => ['layui-btn-danger', 'layui-btn-xs'],
                'confirm' => '确定要删除数据吗?',
            ]);

        $addBtn = new Btn();
        $addBtn->setLabel('添加');
        $addBtn->setUrl('add');
        $addBtn->setIcon('layui-icon layui-icon-add-1');
        $page->setActionAdd($addBtn);

        $page->setTemplate('config:lists');

    }

    protected function configListField(ListField $list)
    {
        $list
            ->addField('id', 'ID', ListText::class)
            ->addField('key', '配置标识', ListText::class)
            ->addField('value', '值', ListText::class)
            ->addField('mark', '说明', ListText::class)
            ->addField('create_time', '创建时间', ListDateTime::class)
            ->addField('update_time', '更新时间', ListDateTime::class)
            ->addField('is_enable', '是否启用', ListSwitch::class)//            ->addField('is_del', '删除', ListSwitch::class)
        ;
    }

    protected function configListWhere(Page $page, Query $query, $alias)
    {
        // 查询顶级分类
        $parents = $this->getModel()->where('parent_id', '=', 0)
            ->where('is_del', '=', 0)
            ->select()->toArray();


        //取得顶级分类的第一条数据
        $firstParentId = 0;
        if ($parents) {
            $firstParentId = $parents[0]['id'];
        }

        //接收用户传递的参数
        $parent_id = request()->get('parent_id', $firstParentId);

        $query
            ->where('parent_id', '=', $parent_id)
            ->where('is_del', '=', '0');

        //顶级分类赋值到页面
        $this->assign('parents', $parents);
        //当前的顶级分类赋值到页面
        $this->assign('parent_id', $parent_id);


    }


    protected function configFormField(PageForm $page)
    {
        $page
            ->addField('parent_id', '上级', FormSelect::class, [
                'query' => Db::table('config')->where('parent_id', '=', 0)->select(),
                'pk' => 'id',//使用查询,的主键
                'property' => 'mark',//查询显示字段
            ])
            ->addField('key', '配置标识', FormText::class, [
                'required' => true
            ])
            ->addField('value', '值', FormText::class, [
                'required' => true
            ])
            ->addField('mark', '说明', FormText::class, [
                'required' => true
            ]);
    }

    /**
     * 删除顶级分类
     * @return JsonAlias
     */
    public function del_parent(): JsonAlias
    {
        try {
            $id = request()->get('id');
            if (empty($id)) throw new ExceptionAlias('缺少参数');

            $res = Db::table($this->getTableName())->where($this->pk, '=', $id)->save(['is_del' => 1]);
            $res += Db::table($this->getTableName())->where('parent_id', '=', $id)->save(['is_del' => 1]);
            return $this->success([
                'res' => $res
            ]);
        } catch (ExceptionAlias $e) {
            return $this->error($e->getMessage());
        }

    }

}
