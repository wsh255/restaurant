<?php

namespace app\admin\controller;


use easyadmin\app\columns\form\FormAutocomplete;
use easyadmin\app\columns\form\FormDateTime;
use easyadmin\app\columns\form\FormDateTimeRange;
use easyadmin\app\columns\form\FormEditor;
use easyadmin\app\columns\form\FormSelect;
use easyadmin\app\columns\form\FormText;
use easyadmin\app\columns\form\FormTextarea;
use easyadmin\app\columns\form\FormUpload;
use easyadmin\app\columns\lists\ListDateTime;
use easyadmin\app\columns\lists\ListImage;
use easyadmin\app\columns\lists\ListSelect;
use easyadmin\app\columns\lists\ListText;
use easyadmin\app\libs\Breadcrumb;
use easyadmin\app\libs\Btn;
use easyadmin\app\libs\ListField;
use easyadmin\app\libs\ListFilter;
use easyadmin\app\libs\Page;
use easyadmin\app\libs\PageForm;
use easyadmin\app\libs\PageList;
use think\db\Query;

class Store extends Admin
{

    protected string $softDeleteField = 'is_del';
    protected string $pageName = '商户';

    protected $levels = [
        ['key' => '1', 'text' => '普通店铺', 'color' => '#dddddd'],
        ['key' => '2', 'text' => '铜牌店铺', 'color' => '#1E9FFF'],
        ['key' => '3', 'text' => '银牌店铺', 'color' => '#5FB878'],
        ['key' => '4', 'text' => '金牌店铺', 'color' => '#FF5722'],
    ];

    protected function configList(PageList $page)
    {
        $params = [];
        if (request()->get('id')) {
            $params = ['referer' => request()->server('HTTP_REFERER')];
        }

        $page->addAction('桌号','admin/StoreTable/lists', [
            'icon' => 'layui-icon layui-icon-release',
            'class' => ['layui-btn-primary', 'layui-btn-xs'],
            'params'=>$params
        ])->addAction('编辑', 'edit', [
            'icon' => 'layui-icon layui-icon-edit',
            'class' => ['layui-btn-primary', 'layui-btn-xs']
        ])->addAction('删除', 'delete', [
            'icon' => 'layui-icon layui-icon-delete',
            'class' => ['layui-btn-danger', 'layui-btn-xs'],
            'confirm' => '确定要删除数据吗?',
        ]);

        $addBtn = new Btn();
        $addBtn->setLabel('添加');
        $addBtn->setUrl('add');
        $addBtn->setIcon('layui-icon layui-icon-add-1');
        $page->setActionAdd($addBtn);
    }

    protected function configListFilter(ListFilter $filter)
    {
        $filter->addFilter('agent_id','代理商',FormSelect::class,[
            'table' => 'agent',
            'pk' => 'id',//使用查询,的主键
            'property' => 'name',//查询显示字段
        ]);
        $filter->addFilter('name','名称',FormText::class);
        $filter->addFilter('contact_name','联系人',FormText::class);
        $filter->addFilter('contact_mobile','联系电话',FormText::class);
    }

    protected function configListJoin(Page $page, Query $query, string $alias)
    {
        $query->join('agent a', "a.id={$alias}.agent_id", 'left');
    }

    protected function configListField(ListField $list)
    {
        $list
            ->addField('id', 'ID', ListText::class)
            ->addField('a.name', '代理商', ListText::class, ['default' => '-'])
            ->addField('intro_cover', '封面', ListImage::class)
            ->addField('name', '名称', ListText::class)
            ->addField('level', '店铺等级', ListSelect::class, [
                'options' => $this->levels
            ])
            ->addField('business_hours_start', '营业开始时间', ListText::class)
            ->addField('business_hours_end', '营业结束时间', ListText::class)
            ->addField('contact_name', '联系人', ListText::class)
            ->addField('contact_mobile', '联系电话', ListText::class)
            ->addField('create_time', '入驻时间', ListDateTime::class)
            ->addField('expired_time', '过期时间', ListDateTime::class);
    }

    protected function configListWhere(Page $page, Query $query, $alias)
    {
        $agentId = request()->get('id');
        if ($agentId) {
            //$query->where("{$alias}.agent_id", '=', $agentId);

            //面包屑增加
            $breadcrumb = Breadcrumb::getInstance();
            $breadcrumb->add('代理商', request()->server('HTTP_REFERER'), '', 2);
        }
    }


    protected function configFormField(PageForm $page)
    {
        $page
            ->addField('agent_id', '代理商', FormAutocomplete::class, [
                'table' => 'agent',
                'pk' => 'id',
                'property' => 'name'
            ])
            ->addField('name', '名称', FormText::class)
            ->addField('intro_cover', '封面', FormUpload::class)
            ->addField('level', '店铺等级', FormSelect::class, [
                'options' => $this->levels
            ])
            ->addField('contact_name', '联系人', FormText::class)
            ->addField('contact_mobile', '联系电话', FormText::class)
            ->addField('contact_qq_wx', 'QQ 微信', FormText::class)
            ->addField('contact_addr', '联系地址', FormText::class)
            ->addField('expired_time', '过期时间', FormDateTime::class, [
//                'default'=>0,
                'in_format' => 'strtotime',
                'format' => 'Y-m-d H:i:s'
            ])
            ->addField('business_hours_start', '营业开始时间', FormDateTimeRange::class, [
                'end_field' => 'business_hours_end',
                'format' => function ($val) {
                    return $val;
                },
                'options' => [
                    'type' => 'time'
                ],
                'required'=>true
            ])
            ->addField('intro_title', '简介', FormTextarea::class)
            ->addField('intro_desc', '详细介绍', FormEditor::class);
    }


}
