<?php

namespace app\admin\controller;


use easyadmin\app\columns\form\FormAutocomplete;
use easyadmin\app\columns\form\FormCurrency;
use easyadmin\app\columns\form\FormText;
use easyadmin\app\columns\lists\ListSwitch;
use easyadmin\app\columns\lists\ListText;
use easyadmin\app\libs\Breadcrumb;
use easyadmin\app\libs\ListField;
use easyadmin\app\libs\Page;
use easyadmin\app\libs\PageForm;
use think\db\Query;

class StoreTable extends Admin
{


    protected string $softDeleteField = 'is_del';
    protected string $pageName = '桌号';


    protected function configListJoin(Page $page, Query $query, string $alias)
    {
        $query->join('store s', "s.id={$alias}.store_id", 'left');
    }

    protected function configListField(ListField $list)
    {
        $list
            ->addField('id', 'ID', ListText::class)
            ->addField('s.name', '商户', ListText::class, ['default' => '-'])
            ->addField('table_name', '桌号', ListText::class)
            ->addField('people_num', '可坐人数', ListText::class)
            ->addField('table_service_fee', '服务费', ListText::class)
            ->addField('is_open', '是否开台', ListSwitch::class,[
                'attr'=>'disabled'
            ])
            ->addField('is_enable', '是否启用', ListSwitch::class);
    }

    protected function configListWhere(Page $page, Query $query, $alias)
    {
        $storeId = request()->get('id');
        if ($storeId) {
            $query->where("{$alias}.store_id", '=', $storeId);

            //面包屑增加
            $breadcrumb = Breadcrumb::getInstance();

            if ($referer = request()->get('referer')) {
                $breadcrumb->add('代理商', $referer, '', 1, 0);
            }

            $breadcrumb->add('商户', request()->server('HTTP_REFERER'), '', 2, 0);

        }
    }


    protected function configFormField(PageForm $page)
    {
        $page
            ->addField('store_id', '商户', FormAutocomplete::class, [
                'table' => 'store',
                'pk' => 'id',
                'property' => 'name'
            ])
            ->addField('table_name', '桌号名称', FormText::class)
            ->addField('people_num', '可坐人数', FormText::class)
            ->addField('table_service_fee', '服务费', FormCurrency::class)
           ;
    }


}
