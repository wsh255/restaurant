<?php

namespace app\admin\controller;

use easyadmin\app\libs\Menu;
use easyadmin\app\libs\MenuItem;
use easyadmin\controller\Admin as easyAdmin;

class Admin extends easyAdmin
{


    protected string $siteName = '<img src="/intro/400x400@50x10.png"  alt="thinkEasyAdmin" style="width: 45px;height: 45px;margin-right: 5px;border-radius: 50%">EasyAdmin';

    /**
     * 设置系统导航
     * @return Menu
     */
    protected function configMenu(): Menu
    {
        return Menu::getInstance()->setItems([

            MenuItem::addItem('首页', 'index/index', ['icon' => 'layui-icon layui-icon-home']),

            MenuItem::addItem('商户', 'javascript:')->setChildren([
                MenuItem::addItem('代理列表', 'agent/lists', ['icon' => 'fa fa-fire']),
                MenuItem::addItem('代理充值记录', 'agentPayLog/lists', ['icon' => 'fa fa-fire']),
                MenuItem::addItem('商户列表', 'store/lists', ['icon' => 'fa fa-fire']),
                MenuItem::addItem('商户桌号', 'storeTable/lists', ['icon' => 'fa fa-fire']),
            ]),

            MenuItem::addItem('菜品', 'javascript:')->setChildren([
                MenuItem::addItem('菜品列表', 'food/lists'),
                MenuItem::addItem('菜品分类', 'category/lists'),
                MenuItem::addItem('菜品标签', 'tag/lists'),
                MenuItem::addItem('点餐口味', 'flavor/lists'),
                MenuItem::addItem('点餐备注', 'taboo/lists'),
            ]),


            MenuItem::addItem('系统', 'javascript:')->setChildren([
                MenuItem::addItem('管理员', 'manage/lists'),
                MenuItem::addItem('系统配置', 'config/lists'),
                MenuItem::addItem('恢复软删除数据', 'index/recovery_del'),
            ]),

        ]);


    }


    protected function updateBefore($data): array
    {
        $data['update_time'] = time();
        return $data;
    }

    protected function insertBefore($data): array
    {
        $data['create_time'] = time();
        $data['update_time'] = time();
        return $data;
    }

}
