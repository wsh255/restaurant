<?php

namespace app\admin\controller;


use easyadmin\app\libs\Page;
use easyadmin\app\libs\PageList;
use think\db\Query;

class Test extends Admin
{

    /**
     * 配置查询语句, 懒得写其他的,都可以在这里实现自定义查询
     * @param Page $page
     * @param Query $query
     * @param $alias
     */
    protected function configListQuery(Page $page, Query $query, $alias)
    {
        // $query->order()
        // $query->group()
        // 更多TP 的查询方法
    }

    /**
     * 配置查询条件 一般配置 join 查询
     * @param Page $page
     * @param Query $query
     * @param string $alias
     */
    protected function configListJoin(Page $page, Query $query, string $alias)
    {
        // $query->order()
        // $query->group()
        // 更多TP 的查询方法
    }

    /**
     * 配置查询语句, 一般是设置查询条件
     * @param Page $page
     * @param Query $query
     * @param $alias
     */
    protected function configListWhere(Page $page, Query $query, $alias)
    {
        // $query->order()
        // $query->group()
        // 更多TP 的查询方法
    }

    /**
     * list 的全局配置, 基本上所有开放的接口都可以这这里设置获取
     * @param PageList $page
     */
    protected function configList(PageList $page)
    {
        //$page->setOrderBy(['id'=>'desc'])
        //$page->getBreadcrumb()->add();
        //$page->setTemplate()
        //代码中输入 $page-> 查看详细
    }

}
