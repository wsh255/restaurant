<?php

namespace app\admin\controller;


use easyadmin\app\columns\form\FormAutocomplete;
use easyadmin\app\columns\form\FormText;
use easyadmin\app\columns\lists\ListDateTime;
use easyadmin\app\columns\lists\ListText;
use easyadmin\app\libs\ListField;
use easyadmin\app\libs\Page;
use easyadmin\app\libs\PageForm;
use easyadmin\app\libs\Verify;
use think\db\Query;

class Tag extends Admin
{


    protected string $softDeleteField = 'is_del';
    protected string $pageName = '菜品标签';


    protected function configListJoin(Page $page, Query $query, string $alias)
    {
        $query->join('store s', "s.id={$alias}.store_id", 'left');
    }

    protected function configListField(ListField $list)
    {
        $list
            ->addField('id', 'ID', ListText::class)
            ->addField('s.name', '商户', ListText::class, ['default' => '-'])
            ->addField('name', '名称', ListText::class)
            ->addField('create_time', '创建时间', ListDateTime::class);
    }


    protected function configFormField(PageForm $page)
    {
        $page
            ->addField('store_id', '商户', FormAutocomplete::class, [
                'table' => 'store',
                'pk' => 'id',
                'property' => 'name',
                'required' => true,
            ])
            ->addField('name', '名称', FormText::class, [
                'required' => true,
            ]);
    }



}
